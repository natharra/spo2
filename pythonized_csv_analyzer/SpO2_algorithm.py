##This is a code port from MAXREFDES117# algorthim.cpp
## This module calculates heart rate and SpO2 levels

#DEFINE SOME CONSTANTS-----
FS = 25 			#Sampling Frequency
BUFFER_SIZE	= FS*4

#uch_spo2_table is approximated as  -45.060*ratioAverage* ratioAverage + 30.354 *ratioAverage + 94.845 ;
#Since this algorithm is aiming for Arm M0/M3. formaula for SPO2 did not achieve the accuracy due to register overflow.
#Thus, accurate SPO2 is precalculated and save longo uch_spo2_table[] per each an_ratio.
spo2_table =[ 95, 95, 95, 96, 96, 96, 97, 97, 97, 97, 97, 98, 98, 98, 98, 98, 99, 99, 99, 99, 
              99, 99, 99, 99, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 
              100, 100, 100, 100, 99, 99, 99, 99, 99, 99, 99, 99, 98, 98, 98, 98, 98, 98, 97, 97, 
              97, 97, 96, 96, 96, 96, 95, 95, 95, 94, 94, 94, 93, 93, 93, 92, 92, 92, 91, 91, 
              90, 90, 89, 89, 89, 88, 88, 87, 87, 86, 86, 85, 85, 84, 84, 83, 82, 82, 81, 81, 
              80, 80, 79, 78, 78, 77, 76, 76, 75, 74, 74, 73, 72, 72, 71, 70, 69, 69, 68, 67, 
              66, 66, 65, 64, 63, 62, 62, 61, 60, 59, 58, 57, 56, 56, 55, 54, 53, 52, 51, 50, 
              49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 31, 30, 29, 
              28, 27, 26, 25, 23, 22, 21, 20, 19, 17, 16, 15, 14, 12, 11, 10, 9, 7, 6, 5, 
              3, 2, 1 ]


#DEFINE SOME VARIABLES----
an_y=[0]*100		#list for copy red led data
an_x=[0]*100	#list for copy ir led data
ir_valley_locs=[0]*15
ratio=[0]*5
#DEFINE SOME FUNCTIONS----
def maxim_HR_SpO2(ir_buffer, ir_buffer_length, red_buffer):
	"returns HR, HR_valid, spo2, spo2_valid, \
	by detecting peaks of PPG and calculating AC/DC amps"
	#outputs:
	#spo2, spo2_valid, HR, HR_valid

	#calc DC mean and substract DC from IR
	ir_mean=0
	for k in range(0,ir_buffer_length):
		ir_mean += ir_buffer[k]
	ir_mean = int( ir_mean/ir_buffer_length )
	#print("   the mean is ", ir_mean)

	#remove DC and invert signal so we can use peak detector as valley detector
	for k in range(0,ir_buffer_length):
		an_x[k] = (-1) * (ir_buffer[k] - ir_mean)
	#print("   got the inverted signal")

	#4-point moving average
	for k in range (0,BUFFER_SIZE-4):
		an_x[k]= int( (an_x[k]+an_x[k+1]+ an_x[k+2]+ an_x[k+3]) / 4 )
	#print("   got th 4pt avg")

	#calculate threshold
	##*****I'm not sure why the values chosen are 30 and 60, if
	##***** could this value could be negative!?
	n_th1=0
	for k in range(0,BUFFER_SIZE):
		n_th1 += an_x[k]
	n_th1 = int( n_th1 / BUFFER_SIZE )
	if n_th1<30: n_th1=30	#min allowed
	if n_th1>60: n_th1=60	#max allowed
	#print("   got the threshold: ", n_th1)

	#use peak detector as valley detector
	##****** Again, not sure why chose min_distance =4, max_num=15.
	##****** also, threshold is being used as min height
	n_npks = maxim_find_peaks(ir_valley_locs, an_x, BUFFER_SIZE, n_th1, 4, 15) #peak_height, peak_distance, max_num_peaks
	peak_interval_sum = 0
	if n_npks>=2 :
		for k in range(1,n_npks): 
			peak_interval_sum += (ir_valley_locs[k] - ir_valley_locs[k-1]) 
		peak_interval_sum = int( peak_interval_sum / (n_npks -1) )
		HR = int( (FS*60) / peak_interval_sum )
		HR_valid = 1;
	else:
		HR=-999		#unable to calculate  because # of peaks are too small
		HR_valid=0
	#print("...got the peaks and HR: ", HR)


	#load raw value again for spo2  RED(=y) and IR(=X)
	for k in range(0,ir_buffer_length):
		an_x[k] = ir_buffer[k]
		an_y[k] = red_buffer[k]

	#find precise min near ir_valley_locs
	#using exact_ir_valley_locs , find ir-red DC andir-red AC for SPO2 calibration an_ratio
	#finding AC/DC maximum of raw
	exact_ir_valley_locs_count = n_npks
	ratio_average =0
	i_ratio_count =0

	for k in range(0,exact_ir_valley_locs_count):
		if ir_valley_locs[k] > BUFFER_SIZE:
			spo2 = -999
			spo2_valid = 0
			return HR, HR_valid, spo2, spo2_valid
	# find max between two valley locations 
	# and use an_ratio betwen AC compoent of Ir & Red and DC compoent of Ir & Red for SPO2 
	for k in range(0, exact_ir_valley_locs_count -1):
		n_y_dc_max= -16777216 
		n_x_dc_max= -16777216
		if (ir_valley_locs[k+1] - ir_valley_locs[k]) > 3:
			for i in range(ir_valley_locs[k], ir_valley_locs[k+1]):
				if (an_x[i]> n_x_dc_max) : 
					n_x_dc_max = an_x[i]
					n_x_dc_max_idx=i
				if (an_y[i]> n_y_dc_max) :
					n_y_dc_max =an_y[i]
					n_y_dc_max_idx=i

			n_y_ac= ( an_y[ir_valley_locs[k+1]] - an_y[ir_valley_locs[k]] ) * ( n_y_dc_max_idx - ir_valley_locs[k] ) #red
			n_y_ac=  int( an_y[ir_valley_locs[k]] + n_y_ac / (ir_valley_locs[k+1] - ir_valley_locs[k]) )   
			n_y_ac=  an_y[n_y_dc_max_idx] - n_y_ac    # subracting linear DC compoenents from raw 

			n_x_ac= ( an_x[ir_valley_locs[k+1]] - an_x[ir_valley_locs[k]] ) * ( n_x_dc_max_idx - ir_valley_locs[k] ) # ir
			n_x_ac=  int( an_x[ir_valley_locs[k]] + n_x_ac / (ir_valley_locs[k+1] - ir_valley_locs[k]) )
			n_x_ac=  an_x[n_y_dc_max_idx] - n_x_ac      # subracting linear DC compoenents from raw 
			
			n_nume= ( n_y_ac * n_x_dc_max) >>7  #prepare X100 to preserve floating value
			n_denom= ( n_x_ac *n_y_dc_max) >>7

			if (n_denom>0  and i_ratio_count<5 and  n_nume!= 0):
				ratio[i_ratio_count] = int( (n_nume*100) / n_denom ) #formular is ( n_y_ac *n_x_dc_max) / ( n_x_ac *n_y_dc_max) ;
				i_ratio_count+=1



	#choose median value since PPG signal may vary from beat to beat
	maxim_sort_ascend(ratio, i_ratio_count)
	middle_idx = int(i_ratio_count/2)

	if middle_idx > 1 :
		ratio_average = int( ( ratio[middle_idx-1] + ratio[middle_idx] ) / 2 ) # use median
	else:
		ratio_average = ratio[middle_idx]

	if (ratio_average>2 and ratio_average<184):
		spo2_calc= spo2_table[ratio_average]
		spo2 = spo2_calc ;
		spo2_valid  = 1;#  float_SPO2 =  -45.060*n_ratio_average* n_ratio_average/10000 + 30.354 *n_ratio_average/100 + 94.845 ;  # for comparison with table
	else:
		spo2 = -999
		spo2_valid = 0;

	#print("   got spo2: ", spo2)
	return HR, HR_valid, spo2, spo2_valid
#end of maxim_HR_SpO2 function


def maxim_find_peaks(locs, x, size, min_height, min_distance, max_num):
	"returns npks, Find at most MAX_NUM peaks separated by MIN_DISTANCE "
	npks = maxim_peaks_above_min_height(locs, x, size, min_height)
	npks = maxim_remove_close_peaks(locs, npks, x, min_distance)
	return min(npks, max_num) 
	#**** why truncate peak number to "max allowed" even if there are more??
#end of maxim_find_peaks function


def maxim_peaks_above_min_height(locs, x, size, min_height):
	"number of peaks (npks). \
	Find peaks above MIN_HEIGHT"
	i = 1
	npks=0 #list to store peaks
	width=0;

	while i < (size-1):
		if (x[i]>min_height and x[i]>x[i-1]): #find left edge of potential peaks
			width=1;
			while ( (i+width)<size and x[i]==x[i+width]): #find flat peaks
				width=width+1
			if( (i+width)<size ):
				if( x[i]>x[i+width] and npks<15 ):		#find right edge of peaks
					locs[npks] = i
					npks += 1
					#for flat peaks, peak location is left edge
					i += width+1
				else:
					i += width
			else:
				i=size
		else:
			i+=1
	#end of while
	return npks
#end of maxim_peaks_above_min_height


def maxim_remove_close_peaks(locs, npks, x, min_distance):
	"number of peaks (npks). \
	Remove peaks separated by less than MIN_DISTANCE"

	#order peaks from large to small
	maxim_sort_indices_descend(x, locs, npks)

	for i in range(-1, npks):
		old_npks = npks
		npks = i+1
		for j in range(i+1, old_npks):
			if(i == -1):
				temp=-1
			else:
				temp = locs[i]
			dist = locs[j] - temp #lag-zero peak of autocorr is at index -1
			if (dist>min_distance or dist< -min_distance):
				locs[ npks ] = locs[j]
				npks+=1

	#reorder indices to ascending order
	maxim_sort_ascend( locs, npks)
	return npks
#end of maxim_remove_close_peaks


def maxim_sort_ascend(x, size):
	"sort List ascending, insertion sort algorithm"
	for i in range(1, size):
		temp = x[i]
		j=i
		while(j>0 and temp<x[j-1]):
			x[j] = x[j-1]
			j-=1
		x[j] = temp
#end of maxim sort ascend 
##****THIS IS literal translation, could optimize by using built in python functions, 
##****or at least get rid of size variable


def maxim_sort_indices_descend(x, indx, size):
	"sort list descending, insertion sort algorithm"
	for i in range(1, size):
		temp = indx[i]
		j=i 
		while(j>0 and x[temp]>x[ indx[j-1]]):
			indx[j] = indx[j-1]
			j-=1
		indx[j]=temp
#end of maxim sort descend













