#this is gonna be a python script
#import SpO2_algorithm
import csv
from SpO2_algorithm import maxim_HR_SpO2

filename = "max_data.csv"
#using n algorithm that parses through 100 sample-buffers
red_buffer=[]
ir_buffer=[]

#load file, make sure there are at least 100 samples
print("Opening file ", filename)
myFile = open(filename)
reader = csv.reader(myFile)

#make sure there are at least 100 samples
n = 0
for row in reader:
    n += 1
print("There are ", n-1, "samples in this file")

if (n-1<100):
    print("Cannot get SpO2 with less than 100 samples")
    exit()

#reload file
myFile.seek(0)
reader = csv.reader(myFile)

#skip first line of headers
print("skipping first row: ")
print( next(reader))

#load first 100 samples into array
for i in range(100):
    row=next(reader)
    red_buffer.append( int(row[1]) )
    ir_buffer.append( int(row[2]) )

#calculate spo2 from first 100 samples
#print("calculate values for first 100 samples")
maxim_HR_SpO2(ir_buffer, len(ir_buffer), red_buffer) 

#Continuously taking samples from MAX30102.  Heart rate and SpO2 are calculated every 1 second
while True:
	#dumping the first 25 sets of samples in the memory and shift the last 75 sets of samples to the top
	for i in range(25,100):
		red_buffer[i-25]=red_buffer[i]
		ir_buffer[i-25]=ir_buffer[i]

	#take 25 sets of samples before calculating the heart rate.
	for i in range(75,100):
		row=next(reader)
		#TODO check for end of file to exit cleanly
		red_buffer[i]= int(row[1]) 
		ir_buffer[i] = int(row[2]) 

		print( red_buffer[i], ir_buffer[i], maxim_HR_SpO2(ir_buffer, len(ir_buffer), red_buffer) )


print("done reading file")
